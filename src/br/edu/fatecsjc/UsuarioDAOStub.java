package br.edu.fatecsjc;

import java.util.ArrayList;
import java.util.List;

public class UsuarioDAOStub implements UsuarioDAO {


    public int contBuscar = 0;
    public int contInserir = 0;

    public UsuarioDAOStub() {
        this.contBuscar = 0;
        this.contInserir = 0;
    }


    @Override
    public Usuario inserir(Usuario u) {
        ++this.contInserir;

        switch (u.getNomeUsuario()){

            case "fabricio":
                u.setId(new Long(1));
                break;
            case "rony":
                u.setId(new Long(2));
                break;
             default:
                 return null;
        }

        return u;
    }

    @Override
    public List<Usuario> buscar(Usuario usuario) {
        ++this.contBuscar;

        List<Usuario> usuarios = new ArrayList<>();
        List<Usuario> usuariosAchados = new ArrayList<>();

        Usuario uNovo = new Usuario();
        uNovo.setId(new Long(1));
        uNovo.setNomeUsuario("fabricio");
        uNovo.setSenha("xyz");

        usuarios.add(uNovo);

        for (Usuario ubd : usuarios){
            if (ubd.getNomeUsuario().equals(uNovo.getNomeUsuario()))
                usuariosAchados.add(ubd);
        }

        return usuariosAchados;
    }
}
