package br.edu.fatecsjc;

import java.util.List;

public interface UsuarioDAO {

    Usuario inserir(Usuario u);

    List<Usuario> buscar(Usuario usuario);
}
